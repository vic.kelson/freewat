
AkvaGIS code overview
=====================

Overview
--------
The AkvaGIS plugin enhances QGIS with hydrochemical and hydrogeological data processing and analysis.
All reference and measurement data is stored in a SQLite database.

The code has been designed following the Object Oriented paradigm. A large effort has been put on avoiding code repetition in order to reduce errors and improve  the maintainability

This entry point for the QGIS plugin is the :py:class:`akvaGIS.akvaGIS` class. It takes care of adding toolbar buttons to QGIS and 
linking them to their respective functionality. 

The toolbar has the following look

.. image:: ToolBar.png

The functionality behind each button is called a 'Feature'. Each Feature, with baseclass :py:class:`feature.AKFeature.AKFeature` can either 
contain other (sub-)Features or/and a form.
 
There are 3 groups of features:

- General Features
    These are features that deal with the AkvaGIS SQLite database and 
    the related QGIS layers, like create/open/close AkvaGIS database. These features correspond to the group of black colored based toolbar buttons
- Hydrochemical Features
    These features manage the selection of geochemical measurements. Different types of 
    plots and maps can be created using the selected samples and measurements.  These features correspond to the  group of blue colored based toolbar buttons
- Hydrogeological Features
    These features manage the selection of hydrogeological measurements to draw plots and maps. These features correspond to the  group of green colored based toolbar buttons

From the hydrochemical and hydrogeological features there are some to generate plots and others to generate maps:

- These are the features to generate plots:

.. inheritance-diagram:: 
    feature.AKFeatureMapStiff  
    feature.AKFeatureEasyQuim
    feature.AKFeatureExcelMix
    feature.AKFeatureIonicBalance
    feature.AKFeaturePiper
    feature.AKFeatureSamplesChemistry
    feature.AKFeatureSamplesHydrogeology
    feature.AKFeatureSAR
    feature.AKFeatureSBD 
    feature.AKFeatureStatQuimet
    feature.AKFeatureStiffPlot
    feature.AKFeatureChemTimePlot
	feature.AKFeatureHydroTimePlot
    :parts: 1    
 
- And these are the features to generate maps:

.. inheritance-diagram:: 
    feature.AKFeatureMapChem
    feature.AKFeatureMapStiff
    feature.AKFeatureMapNormative
    feature.AKFeatureMapChem
    feature.AKFeatureMapHydro
    feature.AKFeatureMapHydroSurface
    :parts: 1

| All database specific settings are managed by the :py:class:`AKSettings.AKSettings` class
| All the presented forms are based on the :py:class:`form.AKForm.AKForm` class.
| All the models used in the (table-)views can be found in the :py:mod:`model` module.

Third Party Libraries
---------------------
AkvaGIS uses different third party libraries. Some of them are distributed with the plugin (inside the :py:mod:`external2` module) and 
others should already be installed in the Python distribution for a correct behaviour of the tool (dependencies).

The following third party libraries are provided with the plugin distribution:

- ChemPlotLib 1.0: 
    A GPL licensed library that draws the chemical plots provided in the plugin: Stiff diagram, Piper diagrams and SAR plots but also standard plots as 1D line plots. It relies on Matplotlib 1.5 and. ChemPlotLib offers extensive customization of plots, title labels, axis, edges sizes and colours can be chosen by users.
- Openpyxl2.3 
    (https://openpyxl.readthedocs.io): A MIT licensed library for reading and writing Excel 2010 xlsx/xlsm/xltx/xltm files.AkvaGIS uses it to export data to MSExcel spreadsheets format.  
- Odfpy 1.3 
    (https://pypi.python.org/pypi/odfpy): A library to read and write OpenDocument v. 1.2 files. AkvaGIS uses it to export data to ODF spreadsheets format. 
- Pyexcel 0.2 
    (https://pyexcel.readthedocs.io): A BSD licensed Python Wrapper that provides one API for reading, manipulating and writing data in csv, ods, xls, xlsx and xlsm files. AkvaGIS uses it to export data to different spreadsheets formats.   

The dependencies of the lib are:

- PyQt4: 
    The Qt version 4 Python wrapper.
- Matplotlib 1.5 
    (http://matplotlib.org/): A Python 2D plotting library which produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms


