feature package
===============

Subpackages
-----------

.. toctree::

    feature.measurements
    feature.results

Submodules
----------

feature.AKFeature module
------------------------

.. automodule:: feature.AKFeature
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureCloseDB module
-------------------------------

.. automodule:: feature.AKFeatureCloseDB
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureCreateDB module
--------------------------------

.. automodule:: feature.AKFeatureCreateDB
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureCustomChart module
-----------------------------------

.. automodule:: feature.AKFeatureCustomChart
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureCustomChartChem module
---------------------------------------

.. automodule:: feature.AKFeatureCustomChartChem
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureCustomChartHydro module
----------------------------------------

.. automodule:: feature.AKFeatureCustomChartHydro
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureDBManagement module
------------------------------------

.. automodule:: feature.AKFeatureDBManagement
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureEasyQuim module
--------------------------------

.. automodule:: feature.AKFeatureEasyQuim
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureExcelMix module
--------------------------------

.. automodule:: feature.AKFeatureExcelMix
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureIonicBalance module
------------------------------------

.. automodule:: feature.AKFeatureIonicBalance
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureMapChem module
-------------------------------

.. automodule:: feature.AKFeatureMapChem
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureMapHydro module
--------------------------------

.. automodule:: feature.AKFeatureMapHydro
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureMapHydroSurface module
---------------------------------------

.. automodule:: feature.AKFeatureMapHydroSurface
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureMapNormative module
------------------------------------

.. automodule:: feature.AKFeatureMapNormative
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureMapStiff module
--------------------------------

.. automodule:: feature.AKFeatureMapStiff
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeaturePiper module
-----------------------------

.. automodule:: feature.AKFeaturePiper
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureSAR module
---------------------------

.. automodule:: feature.AKFeatureSAR
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureSBD module
---------------------------

.. automodule:: feature.AKFeatureSBD
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureSamples module
-------------------------------

.. automodule:: feature.AKFeatureSamples
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureSamplesChemistry module
----------------------------------------

.. automodule:: feature.AKFeatureSamplesChemistry
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureSamplesHydrogeology module
-------------------------------------------

.. automodule:: feature.AKFeatureSamplesHydrogeology
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureStatQuimet module
----------------------------------

.. automodule:: feature.AKFeatureStatQuimet
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureStiffPlot module
---------------------------------

.. automodule:: feature.AKFeatureStiffPlot
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureChemTimePlot module
------------------------------------

.. automodule:: feature.AKFeatureChemTimePlot
    :members:
    :undoc-members:
    :show-inheritance:

feature.AKFeatureHydroTimePlot module
-------------------------------------

.. automodule:: feature.AKFeatureHydroTimePlot
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: feature
    :members:
    :undoc-members:
    :show-inheritance:
