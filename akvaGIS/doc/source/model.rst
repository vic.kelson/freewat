model package
=============

Submodules
----------

model.PlotSettingsModel module
------------------------------

.. automodule:: model.PlotSettingsModel
    :members:
    :undoc-members:
    :show-inheritance:

model.QueryManagerSamplesModel module
-------------------------------------

.. automodule:: model.QueryManagerSamplesModel
    :members:
    :undoc-members:
    :show-inheritance:

model.SamplesMeasurementsModel module
-------------------------------------

.. automodule:: model.SamplesMeasurementsModel
    :members:
    :undoc-members:
    :show-inheritance:

model.SavedQueryMeasurementsModel module
----------------------------------------

.. automodule:: model.SavedQueryMeasurementsModel
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: model
    :members:
    :undoc-members:
    :show-inheritance:
