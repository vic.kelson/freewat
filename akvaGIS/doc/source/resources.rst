resources package
=================

Submodules
----------

resources.resources module
--------------------------

.. automodule:: resources.resources
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: resources
    :members:
    :undoc-members:
    :show-inheritance:
