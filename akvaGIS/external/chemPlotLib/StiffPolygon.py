#
#coding=utf-8
#Copyright (C) 2015 Barcelona Science
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 Barcelona Science
:authors: L.M. de Vries, A. Nardi
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
def stiffPolygonLocalCoords(values, xCenterCoord = 0.0, yCenterCoord = 0.0, scaleX = 1.0, yLength = 10.0):    
    
    dataPlot = {}
    dataPlot["Na"] = {"x":-values["Na"]/scaleX, "y": yLength}
    dataPlot["Mg"] = {"x":-values["Mg"]/scaleX, "y": 0}
    dataPlot["Ca"] = {"x":-values["Ca"]/scaleX, "y": -yLength}
    dataPlot["Cl"] = {"x":values["Cl"]/scaleX, "y": yLength}
    dataPlot["SO4"] = {"x":values["SO4"]/scaleX, "y": 0}
    dataPlot["HCO3"] = {"x":values["HCO3"]/scaleX, "y": -yLength}
            
    coords = [[xCenterCoord+dataPlot["Na"]["x"], yCenterCoord+dataPlot["Na"]["y"]],
            [xCenterCoord+dataPlot["Mg"]["x"], yCenterCoord+dataPlot["Mg"]["y"]],
            [xCenterCoord+dataPlot["Ca"]["x"], yCenterCoord+dataPlot["Ca"]["y"]],
            [xCenterCoord+dataPlot["HCO3"]["x"], yCenterCoord+dataPlot["HCO3"]["y"]],
            [xCenterCoord+dataPlot["SO4"]["x"], yCenterCoord+dataPlot["SO4"]["y"]],
            [xCenterCoord+dataPlot["Cl"]["x"], yCenterCoord+dataPlot["Cl"]["y"]]
            ]
    return coords

def horizontalLinesCoords(xRange, yLength):
    lines = {}
    lines["top"] = [[-xRange, xRange],[yLength, yLength]]
    lines["middle"] = [[-xRange, xRange],[0.0, 0.0]]
    lines["bottom"] = [[-xRange, xRange],[-yLength, -yLength]]
    return lines
    
def verticalLineCoords(xRange, yLength):
    line = [[0.0, 0.0],[yLength*1.1, -yLength*1.1]]
    return line

def labelsInfo(xRange, yLength):
    xExtra = xRange * 0.1
    xMin = -xRange - xExtra
    xMax = -xMin
    yMin = -yLength
    yMiddle = 0.0
    yMax = yLength

    dataPlot ={}
    dataPlot["Na"] = {"x": xMin, "y": yMax, "label": "Na", "posVertical":"center", "posHorizontal":"right" }
    dataPlot["Mg"] = {"x": xMin, "y": yMiddle,"label": "Mg", "posVertical":"center", "posHorizontal":"right" }
    dataPlot["Ca"] = {"x": xMin, "y": yMin,"label": "Ca", "posVertical":"center", "posHorizontal":"right" }
    dataPlot["Cl"] = {"x": xMax, "y": yMax,"label": "Cl", "posVertical":"center", "posHorizontal":"left" }
    dataPlot["SO4"] = {"x": xMax, "y": yMiddle,"label": "SO4", "posVertical":"center", "posHorizontal":"left" }
    dataPlot["HCO3"] = {"x": xMax, "y": yMin,"label": "HCO3", "posVertical":"center", "posHorizontal":"left" }        
    return dataPlot

def plotRange(xRange, yLength):
    coords = [-xRange, xRange, -yLength*1.1, yLength*1.1]
    return coords

    
    
    
    
