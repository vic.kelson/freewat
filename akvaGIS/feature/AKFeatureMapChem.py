#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.QtGui import QIcon

from AKFeatureCustomChartChem import AKFeatureCustomChartChem
from results.AKFeatureResultsMapChem import AKFeatureResultsMapChem

class AKFeatureMapChem (AKFeatureCustomChartChem):
    '''
    This feature generates map with hydrochemical measurements information. The user needs to decide if he/she is 
    interested in the first,last,average,minimum or maximum value of all the values for a specific parameter. 
    '''
    def __init__(self, settings, iface, parent=None):
        super(AKFeatureMapChem, self).__init__(settings, iface, windowTitle=u'Parameter Map Measurements', parent=parent)
        self.m_icon = QIcon(settings.getIconPath('parameterMap.png'))
        self.m_text = u'Chemical Parameter Map'
        self.m_measurementsSubFeature.m_parametersAreImposed = False
        self.m_plotSubFeature = AKFeatureResultsMapChem(self.m_settings, self.iface, parent = self.m_measurementsSubFeature.m_form.window())

