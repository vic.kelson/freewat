'''
Created on 27/10/2015

@author: Albert Nardi
'''
from PyQt4 import QtGui, QtCore
from qgis.gui import QgsColorButtonV2
from PyQt4.QtGui import QColor

class AkvaColorButton(QgsColorButtonV2):
    
    def __init__(self, *args, **kwargs):
        QgsColorButtonV2.__init__(self,*args,**kwargs)
        
        self.m_line_edit = None
        
    def setConnections(self, plotSettingsModel, lineEditWidget, title_font_color_col):
        
        self.plotSettingsModel = plotSettingsModel
        self.lineEditWidget = lineEditWidget
        self.title_font_color_col = title_font_color_col  
        
                 
        self.colorChanged.connect(self.onColorPickerChanged)
        self.lineEditWidget.editingFinished.connect(self.onColorTextEditChanged)
      
    
    def onColorPickerChanged(self, color):
        mod = self.plotSettingsModel
        mod.setData(mod.index(0, self.title_font_color_col), color.name())
    
    def onColorTextEditChanged(self):
        self.setColor(QColor(self.lineEditWidget.text()))