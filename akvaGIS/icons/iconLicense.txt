Icons used have different sources:

From Breeze KDE icons(LGPL 3+ https://phabricator.kde.org/diffusion/BREEZE/ Copyright (C) 2014 Uri Herrera <uri_herrera@nitrux.in> and others)
-> document-close.png
-> document-new.png
-> document-open.png

Slightly modified from Breeze KDE icons (LGPL 3+ https://phabricator.kde.org/diffusion/BREEZE/ Copyright (C) 2014 Uri Herrera <uri_herrera@nitrux.in> and others)
-> edit-database-chem.png
-> timePlot.png
-> edit-select-chem.png
-> edit-select-hydro.png

The above icons are based on:
-> chronometer.svg -> from Breeze (LGPL 3+ - https://phabricator.kde.org/diffusion/BREEZE/ )
-> tool-pointer.svg -> from Breeze (LGPL 3+ - https://phabricator.kde.org/diffusion/BREEZE/ )
-> network-server-database.svg -> from Breeze (LGPL 3+ - https://phabricator.kde.org/diffusion/BREEZE/ )

From BarcelonaScience (www.barcelonascience.com) icons(GPL v2 or later):
-> easyQuim.png
-> statQuimet.png
-> mix.png
-> piper.png
-> SAR.png
-> SBD.png
-> stiffMap.png
-> stiffPlot.png
-> geoMap.png
-> hydroMap.png
-> ionicBalance.png
-> parameterMap.png
-> parameterNormativeMap.png





