#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.Qt import Qt, QAbstractTableModel, QModelIndex

class PlotSettingsModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(PlotSettingsModel, self).__init__(parent)
        self.dict_data = {}                 
        
    def setInitialData(self, dict_data):
        self.dict_data = dict_data

        counter=0
        self.fieldToColumnPosition = {}
        self.columnPositionToField = []
        for family in self.dict_data:
            self.fieldToColumnPosition[family]={}
            for id in self.dict_data[family]:
                self.fieldToColumnPosition[family][id] = counter
                self.columnPositionToField.append([family, id])
                counter +=1                    
        
        self.numberOfColumns = len(self.columnPositionToField)       
        self.numberOfRows = 1

    def rowCount(self, parent=QModelIndex()):
        return self.numberOfRows

    def columnCount(self, parent=QModelIndex()):
        return self.numberOfColumns               
    
    def columnFromField(self, family, id):
        return self.fieldToColumnPosition[family][id]        
    
    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return None
        
        column = index.column()
        field = self.columnPositionToField[column]
        value = self.dict_data[field[0]][field[1]]["value"]

        return value

    def setData(self, index, value, role=Qt.EditRole):
        if(not index or not index.isValid()):
            return False
        
        column = index.column()
        field = self.columnPositionToField[column]
        self.dict_data[field[0]][field[1]]["value"] = value
        self.dataChanged.emit(index, index)
        return True
    
    def getDictData(self):
        return  self.dict_data        