#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from PyQt4.Qt import QSqlQueryModel, Qt, QSqlQuery
from PyQt4 import QtCore

class QueryManagerSamplesModel(QSqlQueryModel): 
    def __init__(self, sqlQuery, pkField, parent=None): 
        super(QueryManagerSamplesModel, self).__init__(parent)
        self.sqlUpdateActiveField = sqlQuery
        self.pkField = pkField
        
    def setDB(self, db):
        self.curDB = db

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if (index.column() != 0):
            return False
        curRow = index.row()
        curRecord = self.record(curRow)
        self.setActive(curRecord.value(self.pkField), value)
        self.reset()
        self.dataChanged.emit(index,index)
        
        return value
    
    def flags(self, index):
        if (index.column() == 0):
            return QtCore.Qt.ItemIsEnabled |QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable

        return  QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    def setActive(self, fieldId, value):
        query = QSqlQuery(self.curDB)
        query.prepare(self.sqlUpdateActiveField)
        query.addBindValue(value)
        query.addBindValue(fieldId)
        query.exec_()
        
    def reset(self):
        self.query().exec_()