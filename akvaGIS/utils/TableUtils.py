#coding=utf-8
#Copyright (C) 2015 IDAEA-CSIC
#
#This program is free software; you can redistribute it and/or modify it under the terms of the 
#GNU General Public License as published by the Free Software Foundation; either version 2 of the License, 
#or (at your option) any later version. This program is distributed in the hope that it will be useful, but 
#ITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
#PURPOSE. See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License along with this program; if not, write to 
#the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
'''
:copyright: Copyright (C) 2015 IDAEA-CSIC
:authors: V. Velasco Mansilla, L.M. de Vries, A. Nardi, R. Criollo, E. Vázquez Suñé
:contact: enric.vazquez@idaea.csic.es
:license: GPL General Public License, version 2 or any later version (http://opensource.org/licenses/GPL-2.0)
'''
from external.odf.style import TableCellProperties
from external.odf.text import P
from external.odf.table import  TableCell

from datetime import datetime

def valuetype(val):
    valuetype="string"
    if isinstance(val,str): valuetype="string"
    if isinstance(val,int): valuetype="float"
    if isinstance(val,float): valuetype="float"
    if isinstance(val,bool): valuetype="boolean"
    if isinstance(val,datetime): valuetype="date"
    
    return valuetype


def addHeader(textdoc, row, val, numCols, styleName):

    tc = TableCell(valuetype="string", numbercolumnsspanned=numCols, numberrowsspanned=1, stylename=styleName)
    tc.addElement(P(text=val))
    row.addElement(tc)     
    addEmptySpacesToRow(row, numCols-1)
    pass   


def addCellInRow(row, value):
    
    if( valuetype(value) == "date"):
        print("hey, i am a date")
        tc = TableCell(valuetype='date',datevalue = value.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3],stylename="ds1")
        tc.addElement(P(text=value.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]))
        row.addElement(tc)
        
    else:

        tc = TableCell(valuetype=valuetype(value), value=value)
        tc.addElement(P(text=value))
        row.addElement(tc)
                 
    pass
 
def addEmptySpacesToRow(row, numSpaces):
    
    for i in range(numSpaces):
        val = ""
        tc = TableCell(valuetype="string")
        tc.addElement(P(text=val))
        row.addElement(tc)        
    
    pass