# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os
from PyQt4 import QtGui, uic
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName, getModelNlayByName, getModelNspecByName, getTransportModelsByName
from freewat.mdoCreate_utils  import createReactionLayer

#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_createRCTLayer.ui') )
#
class CreateReactionLayerDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)
        # link the flow model with related transport model(s):
        self.cmbFlowName.currentIndexChanged.connect(self.reloadFields)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).setText("Run")
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.createReaction)
        self.manageGui()
##
    def manageGui(self):
        self.cmbFlowName.clear()
        layerNameList = getVectorLayerNames()
        layerNameList.sort()

        # Retrieve modelname and pathfile List
        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)

        self.cmbFlowName.addItems(modelNameList)

        # fill the Grid combobox with only the _grid layer
        grid_layers = []
        for nametemp in layerNameList:
            if '_grid' in nametemp:
                grid_layers.append(nametemp)

        self.cmbGridLayer.addItems(grid_layers)

    def reloadFields(self):
        # Insert list of transport model(s) according with selected flow model:
        self.cmbTransportName.clear()
        layerNameList = getVectorLayerNames()

        list_of_models = getTransportModelsByName(self.cmbFlowName.currentText())
        self.cmbTransportName.addItems(list_of_models)

##
##
    def reject(self):
        QDialog.reject(self)
##
    def createReaction(self):

        # ------------ Load input data  ------------
        #
        modelName = self.cmbFlowName.currentText()
        transportName = self.cmbTransportName.currentText()

        # Remark: pathfile from model table and number of stress periods (nsp) from time table
        (pathfile, nsp ) = getModelInfoByName(modelName)
        nlay = getModelNlayByName(modelName)
        (nspec, mob, mass) = getModelNspecByName(modelName, transportName)

        # dbName
        dbName = os.path.join(pathfile, modelName + '.sqlite')

        #
        gridlayer = getVectorLayerByName(self.cmbGridLayer.currentText())
        newName  = self.txtName.text()


        # Create a REACTION Layer
        createReactionLayer(gridlayer, newName, pathfile, modelName, nlay, nspec)

        self.reject()
