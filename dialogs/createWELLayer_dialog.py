# -*- coding: utf-8 -*-

#******************************************************************************
#
# Freewat
# ---------------------------------------------------------
#
#
# Copyright (C) 2014 - 2015 Iacopo Borsi (iacopo.borsi@tea-group.com)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************


from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
import os
from PyQt4 import QtGui, uic
from freewat.freewat_utils import getVectorLayerByName, getVectorLayerNames, getModelsInfoLists, getModelInfoByName
from freewat.mdoCreate_utils  import createWellLayer, createModelLayer
from freewat.sqlite_utils import getTableNamesList
#
#
FORM_CLASS, _ = uic.loadUiType(os.path.join( os.path.dirname(__file__), 'ui/ui_createWELLayer.ui') )
#
#class CreateWELayerDialog(QDialog, Ui_CreateWELLayerDialog):
class CreateWELayerDialog(QDialog, FORM_CLASS):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)
        self.buttonBox.rejected.connect(self.reject)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.createWEL)

        self.label.setEnabled(False)
        self.cmbWelLayer.setEnabled(False)
        self.manageGui()

##
##
    def manageGui(self):
        self.cmbModelName.clear()
        self.cmbGridLayer.clear()
        self.cmbWelLayer.clear()
        layerNameList = getVectorLayerNames()
        layerNameList.sort()


        # Retrieve modelname and pathfile List
        (modelNameList, pathList) =  getModelsInfoLists(layerNameList)

        self.cmbModelName.addItems(modelNameList)

        # fill the Grid combobox with only the _grid layer
        grid_layers = []
        for nametemp in layerNameList:
            if '_grid' in nametemp:
                grid_layers.append(nametemp)

        self.cmbGridLayer.addItems(grid_layers)

        # create a list of only vector line layers loaded in the TOC
        layerList = QgsMapLayerRegistry.instance().mapLayers()
        pointList = []
        for k, v in layerList.items():
            # check if vector
            if v.type() == 0:
                # check if line
                if v.wkbType() == 1:
                    pointList.append(v.name())

        for p in pointList:
            self.cmbWelLayer.addItem(p)

    def reject(self):
        QDialog.reject(self)
##
    def restoreGui(self):
        self.progressBar.setFormat("%p%")
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(0)
        self.cancelButton.clicked.disconnect(self.stopProcessing)
        self.okButton.setEnabled(True)
##
    def createWEL(self):

        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.progressBar.setValue(0)

        QApplication.processEvents()

        # ------------ Load input data  ------------
        modelName = self.cmbModelName.currentText()
        gridLayer = getVectorLayerByName(self.cmbGridLayer.currentText())
        welLayer = getVectorLayerByName(self.cmbWelLayer.currentText())

        # Remark: pathfile from model table and number of stress periods (nsp) from time table
        (pathfile, nsp ) = getModelInfoByName(modelName)

        # Retrieve the name of the new layer from Text Box
        name = self.textEdit.text()

        # Retrieve the information of the model and the name
        dbName = os.path.join(pathfile, modelName + '.sqlite')
        tableList = getTableNamesList(dbName)
        layerName = name + "_well"

        if layerName in tableList:
            QMessageBox.warning(self, self.tr('Warning!'), self.tr('Table %s already exists!' % layerName))
            return


        if self.point_check.isChecked():
            #print 'spuntato'
            for i in gridLayer.getFeatures():
                for l in welLayer.getFeatures():
                    if i.geometry().intersects(l.geometry()):
                        gridLayer.select(i.id())

        # Create WEL layer
        createWellLayer(gridLayer, name, pathfile, modelName, nsp)

        self.progressBar.setMaximum(100)


        #Close the dialog window after the execution of the algorithm
        self.reject()
