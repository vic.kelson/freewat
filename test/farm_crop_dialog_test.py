# coding = utf-8
import sys
import os
import sqlite3
from tempfile import gettempdir
from qgis.core import *
from PyQt4.QtGui import QDialogButtonBox
from .utils import create_test_model
from ..dialogs.farm_crop_dialog import  CreateFarmCrop
from ..dialogs.createFarmCrop_dialog import createFarmCropSoilDialog
from ..dialogs.createCropTable_dialog import CreateCropTableDialog

app = QgsApplication(sys.argv, True)
QgsApplication.initQgis()

try:
    dlg = CreateFarmCrop(iface=None)
    assert(False) # should trigger an exception because model is missiing
except RuntimeError as e:
    assert(str(e).find('create a MODEL before') != -1)

dbfile = os.path.join(gettempdir(), 'mymodel.sqlite')
create_test_model(dbfile, app)

dlg = CreateCropTableDialog(iface=None)
dlg.tableBox.setChecked(True)
dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)

app.processEvents()

dlg = CreateFarmCrop(iface=None)
dlg.updateButton.clicked.emit(True)

app.processEvents()

if len(sys.argv) > 1:
    dlg.show()
    app.exec_()
else:
    dlg.buttonBox.button(QDialogButtonBox.Ok).clicked.emit(True)
    app.processEvents()

