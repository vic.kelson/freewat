# -*- coding: utf-8 -*-
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QMenu


# Initialize Qt resources from file resources.py
import sys, os
# add path
sys.path.append('C:\\Users\\iacopo.borsi\\.qgis2\\python\\plugins\\freewat')


# Import the code for the dialog
from flopyaddon import ModflowUsb
from flopy.modflow import *
import numpy as np


# Load Modflow Model
namefile = 'C:\\Users\\iacopo.borsi\\ex_farmtest\\t_farm_v1.nam'
ml = Modflow.load(namefile, version='mf2005', model_ws='C:\Users\iacopo.borsi\ex_farmtest', exe_name = 'C:\modflow\mf2005.exe')

#namefile = 'C:\Users\iacopo.borsi\SIDGRID\Zonbud.3_01\Data\Zbtest.nam'
#ml = Modflow.load(namefile, version='mf2005', model_ws='C:\Users\iacopo.borsi\SIDGRID\Zonbud.3_01\Data')

nrow, ncol, nlay, nper = ml.nrow_ncol_nlay_nper

# Create a cell  dictionary
cell_dict = {}
mu_dict = {}
cell_dict[1] = [6,3]
cell_dict[2] = [2,4]
mu_dict[1] = 0
mu_dict[2] = 0.0001
cin_dict = {}

for kper in range(nper):
    cin_dict[kper] = np.zeros(shape = (nrow, ncol))

cin_dict[0][1:3][0] = 1.0


# Create the USB object
usb = ModflowUsb(ml, cell_dict = cell_dict, cin_dict = cin_dict, mu_dict = mu_dict)
# and run USB
usb.write_uzf_gages()
usb.run_model()

cout = usb.cout_dict
